#
# Be sure to run `pod lib lint MBOXSdk.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "MBOXSdk"
  s.version          = "0.3.6"
  s.summary          = "SDK for http://mbox.pro"
  s.description      = <<-DESC
SDK for http://mbox.pro portal ios applications
                       DESC

  s.homepage         = "http://mbox.pro"
s.license          =  {
:type => 'Copyright',
:text => <<-LICENSE
Copyright 2015 MBox. All rights reserved.
LICENSE
}
  s.author           = { "Marianna Demchenko" => "allisable@yandex.ru" }
  s.source           = {:git => 'https://bitbucket.org/allisable88/mboxsdk'}

  s.platform     = :ios, '7.1'
  s.requires_arc = true



s.source_files = 'MBOX/*.h'
s.preserve_paths = 'MBOX'
s.vendored_libraries = 'MBOX/*.a'

s.resources = 'MBOX/*.bundle'

s.library = 'MBOXSdk'
#s.xcconfig = { 'LIBRARY_SEARCH_PATHS' => '"$(PODS_ROOT)/MBOXSdk/MBOX"' }
s.framework    = 'SystemConfiguration'
s.framework    = 'CoreData'
s.framework    = 'AVFoundation'
s.framework    = 'CoreMedia'
s.framework    = 'MobileCoreServices'
s.framework    = 'Security'
s.framework    = 'CoreGraphics'
s.framework    = 'Foundation'
s.framework    = 'AudioToolbox'

s.library = 'xml2'
s.dependency 'AFNetworking'
s.dependency 'Google/Analytics'

end
