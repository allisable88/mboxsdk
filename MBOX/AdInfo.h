//
//  AdView.h
//  DigitalBoxSDKTest
//
//  Created by Марианна Демченко on 27.11.15.
//  Copyright © 2015 AlterEgo Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

enum AdType
{
    //кликабельный баннер
    AdTypeBanner=0,
    //кликабельный текстовой блок
    AdTypeActiveText=1,
    //некликабельный текстовой блок
    AdTypeStaticText=2
};


@protocol AdInfoDelegate <NSObject>
@required

- (void) adClosed;
- (void) adSkipped;
- (void) adLinkTapped;
- (void) showSkipAdInfo;
- (void) showCloseAdInfo;
@end

@interface AdInfo : NSObject
//Булево значение (YES/NO), указывающее возможен ли пропуск рекламы. Если равно YES, необходимо отображать таймер обратного отсчета до появления кнопки пропуска рекламы.
@property (nonatomic, readonly) BOOL isSkippable;

///Тип объявления, определяющий его внешний вид и интерактивность
@property (nonatomic, readonly) enum AdType type;

//Целое число секунд, оставшееся до конца воспроизведения баннера. Необходимо отображать информацию об оставшемся времени воспроизведения рекламы в интерфейсе приложения. Рекомендуемый формат: “Реклама mm:ss”
@property (nonatomic, readonly) long timeRemaining;

//Целое число секунд, оставшееся до возможности пропуска рекламного объявления.
@property (nonatomic, readonly) long timeBeforeSkip;

//Полный URL для перехода по нажатию на активный текст или
@property (strong, nonatomic, readonly) NSURL *link;

//Графический ресурс (баннер) для отрисовки в приложении.
@property (strong, nonatomic, readonly) UIImage *banner;


//Текст для отображения. Данное поле должно использоваться лишь в тех случаях, когда поле type имеет значения AdTypeActiveText или AdTypeStaticText. Приложение должно явным образом показывать пользователю, когда текст является кликабельным.
@property (strong, nonatomic, readonly) NSString *text;



//Метод, который должен быть вызван при нажатии на UI элемент для закрытия (Х) рекламного объявления (только при уже возникшем событии showClose). В момент вызова этого метода, UI элементы объявления должны быть скрыты. После вызова этого метода воспроизведение аудиообъявления будет остановлено, а поле activeAd у объекта SDK приобретет значение nil, и приложение может воспроизводить собственный контент.
- (void) registerClose;

//Метод, который должен быть вызван при каждом нажатии на баннер и переходе по ссылке (link). Нажатие на баннер не должно приводить к скрытию рекламного объявления.

- (void) registerClick;
// Метод, который должен быть вызван при нажатии на UI элемент для пропуска рекламного объявления (только при isSkippable = true и уже возникшем событии showSkip). В момент вызова этого метода, UI элементы объявления должны быть скрыты. После вызова этого метода воспроизведение аудиообъявления будет остановлено, а поле activeAd у объекта SDK приобретет значение nil, и приложение может воспроизводить собственный контент.

- (void) registerSkip;
- (void) endAllAdActivities;
- (void) pause;
- (void) resume;
-(id)initWith:(id)delegate Link:(NSURL*)link StartTime:(long)startTime SkipTime:(long)skipTime SkipTime2:(long) skipTime2 Banner:(UIImage*) banner  IsClickable:(Boolean)isClickable ShowBaner:(Boolean) bannerIsActive Duration :(long) duration Text:(NSString*) linkTxt CanSkip:(Boolean)canSkip;
@end
