//
//  mbox_sdk.h
//  mbox.sdk
//
//  Created by Marianna Demchenko on 16.12.15.
//  Copyright © 2015 AlterEgo Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Station.h"
#import "Track.h"
#import "Rubric.h"
#import "AdInfo.h"
@protocol ChannelDelegate <NSObject>
@optional
//station moved on to next track. Please note, that this may change getPlaylist() call result. If you are showing station playlist at the time of event, call this method again to keep playlist data actual
- (void) eventTrackChangedChannel;
@end

@protocol MboxSDKDelegate <NSObject>
@optional
//ad playback finished (if you have ads playback turned on)
- (void) eventAdFinished;
//ad playback started (if you have ads playback turned on)
- (void) eventAdStarted;
//station moved on to next track. Please note, that this may change getPlaylist() call result. If you are showing station playlist at the time of event, call this method again to keep playlist data actual
- (void) eventTrackChanged;
//station playback stopped, because of emptying audio buffer or user-initiated pause
- (void) eventStopped;
//station playback successfully started
- (void) eventStarted;
//audio buffer is empty, playback stopped temporary due to loading audio data from server
- (void) eventBufferig;
@required
//advertising started
- (void) adStarted;
//advertising loaded
- (void) adLoaded;
//advertising stopped
- (void) adStopped;
// you can show skip button
- (void) showSkip;
// you can show close button
- (void) showClose;
@end

@protocol UniversalPlayerDelegate <NSObject>
@required
//успешно загружено
- (void) adStarted;
- (void) adLoaded;
- (void) adStopped;
- (void) showSkip;
- (void) showClose;
@end

@protocol MboxPlayerDelegate <NSObject>
@optional
//ad playback finished (if you have ads playback turned on)
- (void) eventAdFinishedPlayer;
//ad playback started (if you have ads playback turned on)
- (void) eventAdStartedPlayer;
//station playback stopped, because of emptying audio buffer or user-initiated pause
- (void) eventStoppedPlayer;
//station playback successfully started
- (void) eventStartedPlayer;
//audio buffer is empty, playback stopped temporary due to loading audio data from server
- (void) eventBufferigPlayer;
@end
//MBOX SDK gives you access to the set of streaming audio stations and their playback (1 station at time). API allows you to build highly customized audio player and UI.
@interface MboxSDK : NSObject<MboxPlayerDelegate, ChannelDelegate, UniversalPlayerDelegate>
{
@public
    id <MboxSDKDelegate> _delegate;
    NSString* _GAtrackingID;
}
//MboxSDKDelegate
@property (nonatomic,strong) id delegate;
//Google tracking
@property (nonatomic,strong) NSString* GAtrackingID;
//get SDK singleton
+ (MboxSDK*) sharedInstance;
//initialize SDK with initUrl given to you. After onLoadCompleteCallback is fired, you can use SDK methods below. Call this method when user has an internet connection, as it loads data from backend.
- (void) init:(NSString*) initUrl success:(void(^)(id entry))success;
//play station.
- (void) play:(Station*) station;
//pause station on Audio Session interruptions
- (void) pause;
//stop station playback.
- (void) stop;
//gets list of stations available for playback
- (NSArray*) getStationList;
//gets list of stations belonging to Rubric. You can get Rubrics list by calling getRubrics
- (NSArray*) getStationByRubric:(Rubric*)rubric;
//gets Rubrics list
- (NSArray*) getRubrics;
//returns current playing track of currently playing station. Returns null if no station is playing. If an ad as playing, track played just before ad is returned.
- (Track*) getCurrentTrack;
//returns current playing stations. Returns null if no station is playing
- (Station*) getCurrentStation;
//returns some station
- (Station*) getStationById:(NSString*)stationId;
@property (nonatomic,strong) AdInfo* activeAd;

//лимит на время кеширования
@property (nonatomic) int timeLimit;
//IDFA
@property (nonatomic,strong) NSString* adDeviceID;
@end



