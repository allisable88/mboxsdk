//
//  Track.h
//  MBOX SDK Test
//
//  Created by Marianna Demchenko on 17.12.15.
//  Copyright © 2015 AlterEgo Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface Track : NSObject
//track duration, in seconds
-(long) getDuration;
//track playback position, in seconds
-(long) getPosition;
//artist name
-(NSString*) getArtist;
//track name
-(NSString*) getName;
//cover image URL. May be null
-(NSURL*) getCoverUrl;
-(UIImage*) getCoverImage;
//flag for current track
-(Boolean) getIsPlayingNow;
+(Track*) initWithDictionary:(NSDictionary*) dictionary;
@end
