//
//  Station.h
//  MBOX SDK Test
//
//  Created by Marianna Demchenko on 17.12.15.
//  Copyright © 2015 AlterEgo Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Track.h"
#import <UIKit/UIKit.h>
@interface Station : NSObject
//Name of station
- (NSString*) getName;
//URL of station’s cover image
- (NSURL*)    getCoverUrl;
- (UIImage*)    getCoverImg;
//Station ID
- (NSString*) getId;
//get current playing track. First call of this method loads data from server, subsequent calls use cached data, when applicable
- (void)      getCurrentTrack:(void(^)(Track* currentTrack))success failed:(void(^)(NSError* err))failed;
//get rubrics’ list. Rubrics’ list is loaded from server after init call
- (NSArray*)  getRubrics;
//get playlist info (a set of soon-to-play tracks. getCurrentTrack and getPlaylist use the same dataset, so if one of them was called earlier during the session, calling the other one for the first time will be fast and won’t request additional data from backend
- (void)      getPlaylist:(void(^)(NSArray* tracks))success failed:(void(^)(NSError* err))failed;
+ (Station*)  initWithRawChannel:(id)channel;
@end
