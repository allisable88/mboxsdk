# MBOXSdk

[![CI Status](http://img.shields.io/travis/Marianna Demchenko/MBOXSdk.svg?style=flat)](https://travis-ci.org/Marianna Demchenko/MBOXSdk)
[![Version](https://img.shields.io/cocoapods/v/MBOXSdk.svg?style=flat)](http://cocoapods.org/pods/MBOXSdk)
[![License](https://img.shields.io/cocoapods/l/MBOXSdk.svg?style=flat)](http://cocoapods.org/pods/MBOXSdk)
[![Platform](https://img.shields.io/cocoapods/p/MBOXSdk.svg?style=flat)](http://cocoapods.org/pods/MBOXSdk)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MBOXSdk is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "MBOXSdk"
```

## Author

Marianna Demchenko, allisable@yandex.ru

## License

MBOXSdk is available under the MIT license. See the LICENSE file for more info.
