//
//  Rubric.h
//  MBOXSDKTest
//
//  Created by Marianna Demchenko on 17.12.15.
//  Copyright © 2015 AlterEgo Labs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Rubric : NSObject
+ (Rubric*)  initWithRawRubric:(id)rubric;
//Rubric Id
- (long) getId;
//Rubric name
- (NSString*) getName;
@end
